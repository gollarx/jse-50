package ru.t1.shipilov.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}
